package com.khanas.calculator;


public class Calculator {
    private CalculatorInterface calculatorInterface;

    public Calculator(final CalculatorInterface calculator) {
        this.calculatorInterface = calculator;
    }

    public final double add(final double a, final double b) {
        return calculatorInterface.add(a, b);
    }

    public final double sqrt(final double a) {
        return calculatorInterface.sqrt(a);
    }

    public final double mult(final double a, final double b) {
        return calculatorInterface.mult(a, b);
    }
}

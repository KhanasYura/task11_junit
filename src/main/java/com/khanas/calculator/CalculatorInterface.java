package com.khanas.calculator;


public interface CalculatorInterface {
    double add(double a, double b);

    double sqrt(double a);

    double mult(double a, double b);

}

package com.khanas.student;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Student {
    private ArrayList<String> student;

    public Student() {
        student = new ArrayList<>();
    }

    public final void add(final String studentName) {
        student.add(studentName);
    }

    public final void remove(final String studentName) {
        if (!student.contains(studentName)) {
            throw new NoSuchElementException();
        }
        student.remove(studentName);
    }

    public final int size() {
        return student.size();
    }
}

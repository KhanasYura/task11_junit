package com.khanas.task;

import java.util.Arrays;
import java.util.Random;

public class Task {
    public final int[] longestSequence(final int[] arr) {
        int length = 0;
        int index = 0;
        int maxLength = 0;
        int maxIndex = 0;
        for (int i = 1; i < arr.length - 1; i++) {
            if (arr[i] > arr[i - 1]) {
                length = 1;
                index = i - 1;
            } else if (arr[i] == arr[i - 1] && arr[i] > arr[index]) {
                length++;
            }
            if (arr[i] > arr[i + 1] && arr[i] > arr[index]) {
                if (maxLength < length) {
                    maxIndex = index + 1;
                    maxLength = length;
                }
                index = 0;
                length = 0;
            }
        }
        return new int[]{maxIndex, maxLength};
    }

    public final int minesweeeper(final int m, final int n, final int p) throws Exception {
        if (m * n <= p) {
            throw new Exception("Wrong p");
        }
        int[][] table = new int[m + 2][n + 2];

        for (int[] row : table) {
            Arrays.fill(row, 0);
        }

        int count = 0;
        while (count != p) {
            Random random = new Random();
            int index1 = random.nextInt(m);
            int index2 = random.nextInt(n);
            if (table[index1 + 1][index2 + 1] == 0) {
                table[index1 + 1][index2 + 1] = 1;
                count++;
            }
        }

        System.out.println(Arrays.deepToString(table));

        int[][] result = new int[m][n];
        int sum = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (table[i + 1][j + 1] != 1) {
                    result[i][j] = table[i + 1][j + 1] + table[i + 1][j]
                            + table[i + 1][j + 2] + table[i][j + 1]
                            + table[i][j] + table[i][j + 2]
                            + table[i + 2][j + 1] + table[i + 2][j]
                            + table[i + 2][j + 2];
                    sum += result[i][j];
                }
            }
        }

        System.out.println(Arrays.deepToString(result));
        return sum;
    }
}

package com.khanas.tasks;

import com.khanas.calculator.Calculator;
import com.khanas.calculator.CalculatorInterface;
import com.khanas.student.Student;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MohitoTest {
    private Logger logger = LogManager.getLogger();
    private Student test = new Student();

    @InjectMocks
    Calculator calculator;

    @Mock
    CalculatorInterface calculatorInterface;

    @Test
    public void addTest() {
        when(calculatorInterface.add(10.0, 10.0)).thenReturn(20.0);
        assertEquals(calculator.add(10.0, 10.0), 20.0);
    }

    @Test
    public void SqrtTest() {
        CalculatorInterface calc = Mockito.mock(CalculatorInterface.class);
        when(calc.sqrt(4.0)).thenReturn(2.0);
        assertEquals(calc.sqrt(4.0), 2.0);
    }

    @Test
    public void multTest() {
        when(calculatorInterface.mult(2.0, 3.0)).thenReturn(6.0);
        assertEquals(calculator.mult(2.0, 3.0), 6.0);
    }

}

package com.khanas.tasks;

import com.khanas.student.Student;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StudentTest {
    private Logger logger = LogManager.getLogger();
    private Student test = new Student();


    @Test
    public void testSize() {
        test.add("Yura");
        test.add("Ivan");
        test.add("Petro");
        logger.info("Size of student array");
        assertEquals(3, test.size(), "Size: " + test.size());
    }

    @Test
    public void testAdd() {
        test.add("Yura");
        test.add("Ivan");
        test.add("Petro");
        logger.info("Add 1 student to list");
        test.add("Oleg");
        assertEquals(4, test.size(), "Size: " + test.size());
    }

    @Test
    public void testRemove() {
        test.add("Yura");
        test.add("Ivan");
        test.add("Petro");
        logger.info("Removing 1 student from list");
        test.remove("Petro");
        assertEquals(2, test.size(), "Size: " + test.size());
    }

    @Test
    public void testRemoveException() {
        test.add("Yura");
        test.add("Ivan");
        test.add("Petro");
        logger.info("Removing student with exception");
        assertThrows(Exception.class, () -> test.remove("Victor"));
    }

}

package com.khanas.tasks;

import com.khanas.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class longestSequenceTest {
    Logger logger = LogManager.getLogger();
    private static int count = 1;

    @BeforeEach
    void before() {
        logger.info("---------Test " + count + "---------");
    }

    @AfterEach
    void after() {
        logger.info("------------------------");
    }

    @Test
    void SequenceTest() {
        count++;
        Task task = new Task();
        int[] arr = new int[]{12, 12, 13, 13, 13, 3, 5, 7, 7, 7, 7, 6};
        int[] result = task.longestSequence(arr);
        assertEquals(7, result[0], "Index start: " + result[0]);
        assertEquals(4, result[1], "Length: " + result[1]);
    }

    @Test
    void SequenceTest2() {
        count++;
        Task task = new Task();
        int[] arr = new int[]{12, 12, 13, 13, 13, 13, 13, 7, 7, 7, 7, 6};
        int[] result = task.longestSequence(arr);
        assertEquals(2, result[0], "Index start: " + result[0]);
        assertEquals(5, result[1], "Length: " + result[1]);
    }

    @RepeatedTest(value = 3, name = "Repeated test {currentRepetition}/{totalRepetitions}")
    public void SequenceTest3() {
        logger.info("     Repeated Test");
        count++;
        Task task = new Task();
        int[] arr = new int[]{12, 12, 13, 13, 13, 7, 7, 7, 7, 7};
        int[] result = task.longestSequence(arr);
        assertEquals(2, result[0], "Index start: " + result[0]);
        assertEquals(3, result[1], "Length: " + result[1]);
    }
}

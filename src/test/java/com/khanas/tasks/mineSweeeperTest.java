package com.khanas.tasks;

import com.khanas.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;


public class mineSweeeperTest {

    private Logger logger = LogManager.getLogger();
    private static int count = 1;

    @Before
    public void beforeTest() {
        System.out.println("---------Test " + count + "---------");
    }

    @After
    public void afterTest() {
        logger.info("------------------------");
    }

    @Test
    public void minesweeeper() throws Exception {
        count++;
        Task task = new Task();
        int m = 2;
        int n = 3;
        int p = 4;
        int result = task.minesweeeper(m, n, p);
        assertTrue(result > 2, "Result " + result);
    }

    @Test
    public void minesweeeperTest2() {
        count++;
        Task task = new Task();
        int m = 2;
        int n = 3;
        int p = 7;
        assertThrows(Exception.class, () -> task.minesweeeper(m, n, p));
    }

}
